#![deny(clippy::all)]
#![allow(dead_code)]

#[macro_use]
extern crate napi_derive;

#[napi(object)]
pub struct AAA {
    pub num_c: u32,
}

#[napi]
fn create_aaa(num: u32) -> AAA {
  AAA {
      num_c: num,
  }
}
